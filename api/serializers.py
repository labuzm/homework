import collections

from peewee import Model
from playhouse.shortcuts import model_to_dict


class BaseSerializer(object):
    def __init__(self, data, fields=None, recurse=True):
        """
        :param data: either object or collection of objects to be serialized
        :param fields: list or set of fields instances that indicates what should be serialized
        :param recurse: indicates if FK relationship should be fallowed
        """
        self._data = None

        self.fields = fields
        self.recurse = recurse
        self.initial_data = data

    @property
    def data(self):
        if self._data:
            return self._data
        self._data = data = self.run_serialization()
        return data

    def _serialize(self, obj):
        assert isinstance(obj, Model)
        return self.serialize(model_to_dict(obj, only=self.fields, recurse=self.recurse))

    def serialize(self, obj_data):
        """
        Hook for performing custom serialization.
        :param obj_data: dict made from object
        """
        return obj_data

    def run_serialization(self):
        if isinstance(self.initial_data, collections.Iterable):
            return [self._serialize(obj) for obj in self.initial_data]
        return self._serialize(self.initial_data)


class UserSerializer(BaseSerializer):
    def serialize(self, obj_data):
        return obj_data['uuid'].int
