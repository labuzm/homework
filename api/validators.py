from uuid import UUID

from voluptuous import Schema, Invalid, MultipleInvalid, And, Range


class BaseValidatorMetaclass(type):
    """
    Simple Metaclass that wraps dict with Schema class to save some typing.
    """
    def __new__(mcs, name, bases, attrs):
        schema = attrs['schema']
        if isinstance(schema, dict):
            attrs['schema'] = Schema(schema, required=True)
        return super(BaseValidatorMetaclass, mcs).__new__(mcs, name, bases, attrs)


class BaseValidator(object, metaclass=BaseValidatorMetaclass):
    schema = None

    def __init__(self, data):
        """
        :param data: dict with data to be validated
        """
        self.initial_data = data

        self.error = None
        self._validated_data = None

    @property
    def data(self):
        if self._validated_data is None:
            raise Exception('You must call ".is_valid()" before calling ".data".')
        return self._validated_data

    def get_schema(self):
        if not isinstance(self.schema, Schema):
            raise Exception('Provided schema is invalid(Got {}).'.format(self.schema))
        return self.schema

    def is_valid(self):
        self._validated_data = self.run_validation()
        return not bool(self.error)

    def run_validation(self):
        schema = self.get_schema()
        try:
            data = schema(self.initial_data)
            return self.validate(data)
        except MultipleInvalid as e:
            self.error = '\n'.join(str(error) for error in e.errors)
        except Invalid as e:
            self.error = str(e)

    def validate(self, data):
        """
        Hook for some extra validation that depends on multiple values at the same time.
        """
        return data


class FriendsUuidValidator(BaseValidator):
    schema = {
        'uuid': And(int, Range(min=0)),
        'f_uuid': And(int, Range(min=0))
    }

    def validate(self, data):
        uuid = data['uuid']
        f_uuid = data['f_uuid']
        if uuid == f_uuid:
            raise Invalid('Provided uuids must not be the same.')

        # cast uuids to appropriate types
        # we use UUID type to store id in the database
        data['uuid'] = UUID(int=uuid)
        data['f_uuid'] = UUID(int=f_uuid)
        return data
