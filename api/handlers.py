from uuid import UUID

import peewee_async
from tornado import gen
from tornado.web import RequestHandler
from tornado.escape import json_encode, json_decode

from db.models import User
from api.serializers import UserSerializer
from api.validators import FriendsUuidValidator


class JsonHandler(RequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.decoded_json = {}

    def prepare(self):
        if self.request.headers.get('Content-Type') != 'application/json':
            message = 'Invalid "Content-Type" header, "application/json" is required.'
            return self.send_error(status_code=400, message=message)

        if not self.request.body:
            return

        try:
            self.decoded_json = json_decode(self.request.body)
        except ValueError:
            self.send_error(status_code=400, message='Unable to parse JSON.')

    def write_error(self, status_code, **kwargs):
        kwargs.pop('exc_info', None)
        if 'message' not in kwargs:
            kwargs['message'] = self._reason
        self.write_json(kwargs)

    def set_default_headers(self):
        self.set_header('Content-Type', 'application/json')

    def write_json(self, data):
        return self.write(json_encode(data))


class ManageRelationshipsHandler(JsonHandler):
    def validate_data(self):
        validator = FriendsUuidValidator(self.decoded_json)
        if validator.is_valid():
            return validator.data
        self.send_error(status_code=400, message=str(validator.error))

    @gen.coroutine
    def post(self):
        data = self.validate_data()
        if data is None:
            return

        from_user, to_user = yield [
            User.create_or_get_async(uuid=data['uuid']),
            User.create_or_get_async(uuid=data['f_uuid'])
        ]
        relationship_obj = yield User.create_relationship(from_user, to_user)
        if relationship_obj:
            return self.write_json(dict(message='The relationship has been created successfully.'))
        self.write_json(dict(message='No relationship created, users are already friends.'))

    @gen.coroutine
    def delete(self):
        data = self.validate_data()
        if data is None:
            return

        success_msg = 'Relationship has been removed successfully.'
        no_rel_msg = 'No relationship removed, users are not friends.'

        users = yield from peewee_async.execute(User.select().where(
            (User.uuid == data['uuid']) | (User.uuid == data['f_uuid']))
        )

        l = len(users)
        if l < 2:
            return self.write_json(dict(message=no_rel_msg))
        elif l > 2:
            raise Exception('This should not happen!')

        from_user, to_user = users[0], users[1]
        result = yield User.remove_relationship(from_user, to_user)
        if result:
            return self.write_json(dict(message=success_msg))
        self.write_json(dict(message=no_rel_msg))


class ListFriendsHandler(JsonHandler):
    @gen.coroutine
    def get(self, uuid):
        try:
            obj = yield User.get_async(User.uuid == UUID(int=int(uuid)))
        except User.DoesNotExist:
            result = []
        else:
            result = UserSerializer((yield obj.friends()), fields=[User.uuid]).data
        self.write_json(dict(friends=result))
