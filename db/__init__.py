import logging
from os import environ

from peewee_async import PooledPostgresqlDatabase


db_config = {
    'user': environ.get('HOMEWORK_DATABASE_USER'),
    'database': environ.get('HOMEWORK_DATABASE_NAME'),
    'password': environ.get('HOMEWORK_DATABASE_PASSWORD')
}
assert all(db_config.values())
db = PooledPostgresqlDatabase(**db_config)
model_registry = []


def register_model(model):
    model_registry.append(model)


def syncdb():
    db.create_tables(model_registry, safe=True)
    logging.info('Database synchronized.')
