import psycopg2
import peewee_async
from tornado import gen
from peewee import Model, UUIDField, ForeignKeyField, Check, SQL

from db import db, register_model


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    uuid = UUIDField(primary_key=True)

    def __str__(self):
        return '<{}@{}>'.format(self.__class__.__name__, self.uuid.int)

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def sort_users(*users):
        return sorted(users, key=lambda x: x.uuid)

    @classmethod
    @gen.coroutine
    def get_async(cls, *args):
        return (yield from peewee_async.get_object(cls, *args))

    @classmethod
    @gen.coroutine
    def create_or_get_async(cls, **kwargs):
        try:
            obj = yield from peewee_async.create_object(cls, **kwargs)
            return obj
        except psycopg2.IntegrityError:
            query = []
            for field_name, value in kwargs.items():
                field = cls._meta.fields[field_name]
                if field.unique or field.primary_key:
                    query.append(field == value)
            obj = yield cls.get_async(*query)
            return obj

    @classmethod
    @gen.coroutine
    def create_relationship(cls, from_user, to_user):
        # prevent duplicates:
        # instead of having duplicated bi-directional relationship,
        # force "from_user" to always be a user with smaller uuid,
        from_user, to_user = cls.sort_users(from_user, to_user)
        try:
            return (yield from peewee_async.create_object(Relationship, from_user=from_user, to_user=to_user))
        except psycopg2.IntegrityError:
            pass

    @classmethod
    @gen.coroutine
    def remove_relationship(cls, from_user, to_user):
        from_user, to_user = cls.sort_users(from_user, to_user)

        try:
            return (yield from peewee_async.execute(
                Relationship.delete().where(
                    Relationship.from_user == from_user,
                    Relationship.to_user == to_user
                )
            ))
        except Relationship.DoesNotExist:
            return

    @gen.coroutine
    def friends(self):
        from_user = (
            User.select()
            .join(Relationship, on=Relationship.to_user)
            .where(Relationship.from_user == self)
        )
        to_user = (
            User.select()
            .join(Relationship, on=Relationship.from_user)
            .where(Relationship.to_user == self)
        )
        return (yield from peewee_async.execute((from_user | to_user).order_by(SQL('uuid'))))


class Relationship(BaseModel):
    class Meta:
        indexes = (
            (('from_user', 'to_user'), True),
        )
        constraints = (
            Check('from_user_id < to_user_id'),
        )

    def __str__(self):
        return '<{}@ from {} to {}>'.format(self.__class__.__name__, self.from_user, self.to_user)

    def __repr__(self):
        return self.__str__()

    from_user = ForeignKeyField(User, related_name='to_people')
    to_user = ForeignKeyField(User, related_name='from_people')


register_model(User)
register_model(Relationship)
