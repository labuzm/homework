# Quick Start
## Code
in order to download repository execute:

    git clone https://labuzm@bitbucket.org/labuzm/homework.git

## Dependencies
to install python dependencies type:

    pip install -r requirements.txt

this code depends on Python3.4 and PostgresSQL, others databases are not supported.
## Database
next step would be creating a PostgreSQL database and exporting credentials to environment variables:

    export HOMEWORK_DATABASE_USER=xxx
    export HOMEWORK_DATABASE_NAME=xxx
    export HOMEWORK_DATABASE_PASSWORD=xxx
    
## Usage
in order to run the server execute:

    python main.py

if you run it for the very first time use --syncdb switch for creating db tables.

    python main.py --syncdb

## Client
see demo_client.client for examples.

## Tests 
tests require own database, so create one and export variables:

    export HOMEWORK_TEST_DATABASE_USER=xxx
    export HOMEWORK_TEST_DATABASE_NAME=xxx
    export HOMEWORK_TEST_DATABASE_PASSWORD=xxx

 in order to run tests execute (while standing in the root directory):
 
    python -m tornado.test.runtests tests