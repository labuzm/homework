import uuid

import peewee_async
from tornado import gen
from playhouse.test_utils import test_database

from db.models import User, Relationship
from tests.utils import DBAsyncTestCase, run_test_on_ioloop


# since all queries all executed in async manner
# there is no easy way to count them
class UserTestCase(DBAsyncTestCase):
    models = [User, Relationship]

    def test_sort_users(self):
        with test_database(self.db, self.models):
            first = User.create(uuid=uuid.UUID(int=10))
            second = User.create(uuid=uuid.UUID(int=100))

            from_user, to_user = User.sort_users(first, second)
            self.assertEqual(from_user, first)
            self.assertEqual(to_user, second)

            # reverse order
            from_user, to_user = User.sort_users(second, first)
            self.assertEqual(from_user, first)
            self.assertEqual(to_user, second)

    @run_test_on_ioloop
    @gen.coroutine
    def test_get_async(self):
        with peewee_async.sync_unwanted(self.db):
            raised = False
            u = uuid.uuid4()
            try:
                # there are no users yet
                yield User.get_async(User.uuid == u)
            except User.DoesNotExist:
                raised = True
            self.assertTrue(raised)

        # create user so we can test get_async
        user = User.create(uuid=u)

        with peewee_async.sync_unwanted(self.db):
            fetched_user = yield User.get_async(User.uuid == u)
        self.assertEqual(user, fetched_user)

    @run_test_on_ioloop
    @gen.coroutine
    def test_create_or_get_async(self):
        u = uuid.uuid4()
        # there are no users yet
        self.assertEqual(len(User.select()), 0)

        with peewee_async.sync_unwanted(self.db):
            created_user = yield User.create_or_get_async(uuid=u)
            fetched_user = yield User.create_or_get_async(uuid=u)

        self.assertEqual(created_user, fetched_user)

    @run_test_on_ioloop
    @gen.coroutine
    def test_create_relationship(self):
        # there are no relationships yet
        self.assertEqual(len(Relationship.select()), 0)

        first_user = User.create(uuid=uuid.uuid4())
        second_user = User.create(uuid=uuid.uuid4())

        with peewee_async.sync_unwanted(self.db):
            relationship = yield User.create_relationship(first_user, second_user)

        # one more time, exactly the same relationship
        with peewee_async.sync_unwanted(self.db):
            second_relationship = yield User.create_relationship(first_user, second_user)

        # second relationships has not been created
        self.assertIsNone(second_relationship)

        # one more time - users in reversed order
        with peewee_async.sync_unwanted(self.db):
            third_relationship = yield User.create_relationship(second_user, first_user)

        # third relationships has not been created
        self.assertIsNone(third_relationship)

        # there is exactly one relationship
        self.assertEqual(Relationship.select(), 1)
        self.assertEqual(Relationship.select()[0], relationship)
        users_tuple = tuple(User.sort_users(first_user, second_user))
        self.assertTrue(users_tuple == (relationship.from_user, relationship.to_user))

    @run_test_on_ioloop
    @gen.coroutine
    def test_remove_relationship(self):
        first_user = User.create(uuid=uuid.uuid4())
        second_user = User.create(uuid=uuid.uuid4())
        yield User.create_relationship(first_user, second_user)
        # there is only one relationships.
        self.assertEqual(len(Relationship.select()), 1)

        # let's remove it
        with peewee_async.sync_unwanted(self.db):
            yield User.remove_relationship(first_user, second_user)

        self.assertEqual(len(Relationship.select()), 0)

        # one more time - non-existing relationship
        with peewee_async.sync_unwanted(self.db):
            yield User.create_relationship(first_user, second_user)

    @run_test_on_ioloop
    @gen.coroutine
    def test_friends(self):
        user1 = User.create(uuid=uuid.uuid4())
        user2 = User.create(uuid=uuid.uuid4())
        user3 = User.create(uuid=uuid.uuid4())

        yield User.create_relationship(user1, user2)
        yield User.create_relationship(user1, user3)
        yield User.create_relationship(user2, user3)

        with peewee_async.sync_unwanted(self.db):
            # user1 should have 2 friends (2 and 3)
            friends = yield user1.friends()
            self.assertEqual(set([friend.uuid for friend in friends]), {user2.uuid, user3.uuid})

            # user2 should have 2 friends (1 and 3)
            friends = yield user2.friends()
            self.assertEqual(set([friend.uuid for friend in friends]), {user1.uuid, user3.uuid})

            # user3 should have 2 friends (1 and 2)
            friends = yield user3.friends()
            self.assertEqual(set([friend.uuid for friend in friends]), {user1.uuid, user2.uuid})
