import os
import json
import asyncio
import unittest

from main import make_app
from tornado import ioloop
from tornado.options import options
from tornado.httpclient import AsyncHTTPClient
from playhouse.test_utils import test_database
from peewee_async import PooledPostgresqlDatabase
from tornado.platform.asyncio import AsyncIOMainLoop

test_db_config = {
    'user': os.environ.get('HOMEWORK_TEST_DATABASE_USER'),
    'database': os.environ.get('HOMEWORK_TEST_DATABASE_NAME'),
    'password': os.environ.get('HOMEWORK_TEST_DATABASE_PASSWORD')
}
assert all(test_db_config.values())
test_db = PooledPostgresqlDatabase(**test_db_config)
options.parse_config_file(os.path.join(os.path.dirname(__file__), 'test.conf'))
AsyncIOMainLoop().install()


class DBAsyncTestCase(unittest.TestCase):
    models = []
    db = test_db

    @classmethod
    def setUpClass(cls):
        super(DBAsyncTestCase, cls).setUpClass()
        cls.loop = asyncio.get_event_loop()
        cls.db.connect()
        cls.loop.run_until_complete(cls.db.connect_async(loop=cls.loop))

    @classmethod
    def tearDownClass(cls):
        cls.loop.run_until_complete(cls.db._async_conn._pool.clear())
        cls.db.close()


class BaseAppAsyncTestCase(DBAsyncTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.app = make_app()
        cls.app.listen(options.port)

    def setUp(self):
        super().setUp()
        self.client = AsyncHTTPClient()

    def fetch(self, url, **kwargs):
        defaults = {
            'method': 'GET',
            'raise_error': False,
            'allow_nonstandard_methods': True,
            'headers': {'Content-Type': 'application/json'},
        }
        defaults.update(kwargs)
        data = defaults.get('body')
        if data is not None and isinstance(data, dict):
            defaults['body'] = json.dumps(data)
        return self.client.fetch(url, **defaults)


def run_test_on_ioloop(fun):
    def wrapper(instance, *args, **kwargs):
        io_loop = ioloop.IOLoop.current()
        # sync/drop db tables on every decorator call
        with test_database(instance.db, instance.models, fail_silently=True):
            return io_loop.run_sync(lambda: fun(instance, *args, **kwargs))
    return wrapper
