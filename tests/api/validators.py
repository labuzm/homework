import unittest

from api.validators import FriendsUuidValidator


class FriendsUuidValidatorTestCase(unittest.TestCase):
    def setUp(self):
        self.validator_cls = FriendsUuidValidator

    def test_valid_data(self):
        validator = self.validator_cls({'uuid': 0, 'f_uuid': 1})

        # we cannot access data if we didn't use '.is_valid()'
        self.assertRaises(Exception, lambda: validator.data)

        self.assertTrue(validator.is_valid())

    def test_missing_data(self):
        validator = self.validator_cls({'f_uuid': 1})
        self.assertFalse(validator.is_valid())

        validator = self.validator_cls({'uuid': 0})
        self.assertFalse(validator.is_valid())

        validator = self.validator_cls({})
        self.assertFalse(validator.is_valid())

    def test_extra_data(self):
        # don't allow extra data
        validator = self.validator_cls({'uuid': 0, 'f_uuid': 1, 'extra': 10})
        self.assertFalse(validator.is_valid())

    def test_range(self):
        validator = self.validator_cls({'uuid': 0, 'f_uuid': -1})
        self.assertFalse(validator.is_valid())

    def test_type(self):
        validator = self.validator_cls({'uuid': 0, 'f_uuid': 'ABC'})
        self.assertFalse(validator.is_valid())

        validator = self.validator_cls({'uuid': 0, 'f_uuid': 5.8})
        self.assertFalse(validator.is_valid())

        validator = self.validator_cls({'uuid': 0, 'f_uuid': None})
        self.assertFalse(validator.is_valid())
