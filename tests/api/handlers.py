import json
import uuid
import peewee_async
from tornado import gen

from db.models import User, Relationship
from tests.utils import run_test_on_ioloop, BaseAppAsyncTestCase


class HomeworkAppTestCase(BaseAppAsyncTestCase):
    models = [User, Relationship]

    @run_test_on_ioloop
    @gen.coroutine
    def test_add_relationship(self):
        # don't allow blocking calls
        with peewee_async.sync_unwanted(self.db):

            # valid request
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={'uuid': 1, 'f_uuid': 2}
            )
            self.assertEqual(result.code, 200)

            # we have created two users and one relationship
            number_of_users = len((yield from peewee_async.execute(User.select())))
            self.assertEqual(number_of_users, 2)

            number_of_relationships = len((yield from peewee_async.execute(Relationship.select())))
            self.assertEqual(number_of_relationships, 1)

            from_user = yield from peewee_async.get_object(User, User.uuid == uuid.UUID(int=1))
            to_user = yield from peewee_async.get_object(User, User.uuid == uuid.UUID(int=2))
            yield from peewee_async.get_object(
                Relationship,
                Relationship.from_user == from_user,
                Relationship.to_user == to_user
            )

            # 2nd valid request, we are trying to add relationship that already exist
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={'uuid': 1, 'f_uuid': 2}
            )
            self.assertEqual(result.code, 200)

            # invalid, no data
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={}
            )
            self.assertEqual(result.code, 400)

            # invalid, the same uuid
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={'uuid': 1, 'f_uuid': 1}
            )
            self.assertEqual(result.code, 400)

            # invalid, missing uuid
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={'f_uuid': 1}
            )
            self.assertEqual(result.code, 400)

            # invalid, wrong type
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': '100'}
            )
            self.assertEqual(result.code, 400)

            # invalid, out of range
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': -1}
            )
            self.assertEqual(result.code, 400)

            # invalid, wrong method type
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='GET',
                body=None
            )
            self.assertEqual(result.code, 405)

    @run_test_on_ioloop
    @gen.coroutine
    def test_remove_relationship(self):
        with peewee_async.sync_unwanted(self.db):

            # add one relationship
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='POST',
                body={'uuid': 1, 'f_uuid': 2}
            )
            self.assertEqual(result.code, 200)

            # valid delete request
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': 2}
            )

            self.assertEqual(result.code, 200)

            # no relationship expected
            number_of_relationships = len((yield from peewee_async.execute(Relationship.select())))
            self.assertEqual(number_of_relationships, 0)

            # 2nd valid delete request, we are trying to delete non-existing relationship
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': 2}
            )
            self.assertEqual(result.code, 200)

            # invalid, no data
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={}
            )
            self.assertEqual(result.code, 400)

            # invalid, the same uuid
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': 1}
            )
            self.assertEqual(result.code, 400)

            # invalid, missing uuid
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'f_uuid': 1}
            )
            self.assertEqual(result.code, 400)

            # invalid, wrong type
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': '100'}
            )
            self.assertEqual(result.code, 400)

            # invalid, out of range
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='DELETE',
                body={'uuid': 1, 'f_uuid': -1}
            )
            self.assertEqual(result.code, 400)

            # invalid, wrong method type
            result = yield self.fetch(
                'http://localhost:8888/apiv1/relationship',
                method='GET',
                body=None
            )
            self.assertEqual(result.code, 405)

    @run_test_on_ioloop
    @gen.coroutine
    def test_list_friends(self):
        with peewee_async.sync_unwanted(self.db):
            # at the beginning there are no users, so there are no friends
            result = yield self.fetch('http://localhost:8888/apiv1/user/0')
            self.assertEqual(result.code, 200)

            content = json.loads(result.body.decode())
            self.assertEqual(content['friends'], [])

            # user "0" adds ten friends
            r = list(range(1, 11))
            for i in r:
                yield self.fetch(
                    'http://localhost:8888/apiv1/relationship',
                    method='POST',
                    body={'uuid': 0, 'f_uuid': i}
                )

            result = yield self.fetch('http://localhost:8888/apiv1/user/0')
            content = json.loads(result.body.decode())
            self.assertEqual(content['friends'], r)

            for i in r:
                result = yield self.fetch('http://localhost:8888/apiv1/user/' + str(i))
                content = json.loads(result.body.decode())
                # relation is symmetric, so each user "i" added by user "0" should also have one friend
                self.assertEqual(content['friends'], [0])

            # delete some relationships
            for i in r[5:]:
                yield self.fetch(
                    'http://localhost:8888/apiv1/relationship',
                    method='DELETE',
                    body={'uuid': 0, 'f_uuid': i}
                )

            result = yield self.fetch('http://localhost:8888/apiv1/user/0')
            content = json.loads(result.body.decode())
            # half of friends have been deleted
            self.assertEqual(content['friends'], r[:5])

            for i in r[:5]:
                result = yield self.fetch('http://localhost:8888/apiv1/user/' + str(i))
                content = json.loads(result.body.decode())
                # half of "r" has one friend
                self.assertEqual(content['friends'], [0])

            for i in r[:5]:
                result = yield self.fetch('http://localhost:8888/apiv1/user/' + str(i))
                content = json.loads(result.body.decode())
                # half of "r" don't have any friend
                self.assertEqual(content['friends'], [0])
