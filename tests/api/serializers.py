import uuid
from tornado import gen

from db.models import User
from api.serializers import UserSerializer
from tests.utils import DBAsyncTestCase, run_test_on_ioloop


class UserSerializerTestCase(DBAsyncTestCase):
    models = [User]

    @run_test_on_ioloop
    @gen.coroutine
    def test_single_object(self):
        u = uuid.uuid4()
        obj = User.create(uuid=u)

        serializer = UserSerializer(obj)
        self.assertEqual(serializer.data, u.int)

    @run_test_on_ioloop
    @gen.coroutine
    def test_multiple_objects(self):
        uuids = [uuid.uuid4() for _ in range(20)]
        objs = [User.create(uuid=u) for u in uuids]

        serializer = UserSerializer(objs)
        self.assertEqual(set(serializer.data), set(u.int for u in uuids))

    def test_wrong_type(self):
        serializer = UserSerializer(1)
        self.assertRaises(AssertionError, lambda: serializer.data)

        serializer = UserSerializer('ABC')
        self.assertRaises(AssertionError, lambda: serializer.data)
