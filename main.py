import logging
import asyncio

from tornado.web import Application, url
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado.options import define, options, parse_command_line

from db import db, syncdb
from api.handlers import ManageRelationshipsHandler, ListFriendsHandler

define("syncdb", default=False, help='create tables')
define("debug", default=False, help='run in debug mode')
define('port', default=8080, help='run on the given port', type=int)


def make_app():
    return Application((
        url(r'/apiv1/user/([0-9]+)', ListFriendsHandler, name='list_friends'),
        url(r'/apiv1/relationship', ManageRelationshipsHandler, name='mange_relationships'),
        ),
        debug=options.debug
    )

if __name__ == '__main__':
    parse_command_line()
    if options.syncdb:
        syncdb()

    AsyncIOMainLoop().install()
    app = make_app()
    app.listen(options.port)
    logging.info('Server started')
    logging.info('Listening on port {}'.format(options.port))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(db.connect_async(loop=loop))
    loop.run_forever()
