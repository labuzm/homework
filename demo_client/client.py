import uuid
import json
from urllib import parse
from itertools import chain
from multiprocessing.pool import ThreadPool

import requests


class Client(object):
    base_url = 'http://localhost:8080/'

    def get_absolute_url(self, url):
        return parse.urljoin(self.base_url, url)

    def process_request(self, url, data, method):
        url = self.get_absolute_url(url)
        method = getattr(requests, method)
        response = method(url, json=data)
        return self.handle_response(response)

    @staticmethod
    def handle_response(response):
        response.raise_for_status()
        return json.loads(response.content.decode('utf-8'))

    def create_relationship(self, u, f_u):
        return self.process_request('apiv1/relationship', {'uuid': u, 'f_uuid': f_u}, method='post')

    def remove_relationship(self, u, f_u):
        return self.process_request('apiv1/relationship', {'uuid': u, 'f_uuid': f_u}, method='delete')

    def list_friends(self, u):
        url = parse.urljoin('apiv1/user/', str(u))
        return self.process_request(url, {}, method='get')


client = Client()
pool = ThreadPool(100)
uuids_pairs = [(uuid.uuid4().int, uuid.uuid4().int) for _ in range(1000)]


# add some relationships
def add_fun(t):
    result = client.create_relationship(*t)
    print(result)

pool.map(add_fun, uuids_pairs)
# one more time to test existing relationships
pool.map(add_fun, uuids_pairs)


# list relationships
def list_fun(t):
    result = client.list_friends(t)
    assert len(result['friends']) == 1
    print(result)

pool.map(list_fun, chain.from_iterable(uuids_pairs))


# remove relationships
def remove_fun(t):
    result = client.remove_relationship(*t)
    print(result)

pool.map(remove_fun, uuids_pairs)
# one more time to test non existing relationships
pool.map(remove_fun, uuids_pairs)


# list relationships again
def list_fun2(t):
    result = client.list_friends(t)
    assert len(result['friends']) == 0
    print(result)

pool.map(list_fun2, chain.from_iterable(uuids_pairs))
